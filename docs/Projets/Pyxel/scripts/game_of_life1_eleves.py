"""
Jeu de la vie avec le module pyxel

https://github.com/kitao/pyxel/blob/main/doc/README.fr.md
"""

import pyxel

# =========================================================
# == CONSTANTES
# =========================================================
LARGEUR = 80
HAUTEUR = 80
VIE = 1
MORT = 0
TEXTE = 2
FONDS = 3
COULEUR = {VIE: 8, MORT: 0, TEXTE: 5, FONDS: 10}

# =========================================================
# == INITIALISATION FENETRE
# =========================================================
pyxel.init(LARGEUR, HAUTEUR, title="jeu de la vie")



# =========================================================
# == FONCTIONS DE GRILLE
# =========================================================
def copie_grille(grille):
    """Renvoie une copie profonde de la grille"""
    

def initialiser_grille(grille, motif):
    """
    Affecte l'état VIE à toutes les cases de grille 
    listées dans motif comme couples (lig, col)
    """


def nombre_voisins_vivants(lig, col):
    """Renvoie le nombre de voisins  vivants de la cellule en (lig, col)"""


def evolution_cellule(grille, lig, col):
    """
    Renvoie le nouvel état (VIE ou MORT)de la cellule en (lig, col) 
    dans grille en fonction de son nombre de voisins vivants
    """


def evolution_grille(grille):
    """
    Crée une copie profonde de grille (génération n)
    Remplit cette copie avec l'évolution de chaque cellule de grille
    Renvoie cette nouvelle grille (génération n + 1)
    """

# =========================================================
# == INITIALISER LE MOTIF
# =========================================================
def charger_fichier(chemin):
    """
    Charger un motif stocké dans un fichier au format Plain
    https://conwaylife.com/wiki/Plaintext
    """
    f = open(chemin)
    ligne1 = f.readline().rstrip()
    titre = " ".join(ligne1.split()[1:])
    motif = []
    for ligne in f:
        if ligne[0] != "!":
            motif.append([c for c in ligne.rstrip()])
    return titre, motif


def motif_dans_grille(lig0, col0, motif, grille):
    """
    Recopie un motif (sous-grille) dans grille à partir de 
    la position (lig0, col0) en coin supérieur gauche
    """
    decodage = {".": MORT, "O": VIE}
    largeur_motif = len(motif[0])
    hauteur_motif = len(motif)
    assert lig0 + hauteur_motif <= len(grille) and col0 + largeur_motif <= len(
        grille[0]
    ), "Motif trop grand"
    for lig in range(lig0, lig0 + hauteur_motif):
        for col in range(col0, col0 + largeur_motif):
            grille[lig][col] = decodage[motif[lig - lig0][col - col0]]


# =========================================================
# == AFFICHAGES MENU / FIN
# =========================================================


def afficher_menu():
    """Affichage du menu d'accueil"""
    pyxel.text(
        int(LARGEUR * 0.25), int(HAUTEUR * 0.25), f"{jeu['nom']}", COULEUR[TEXTE]
    )
    pyxel.text(
        int(LARGEUR * 0.1), int(HAUTEUR * 0.5), "Press s to start", COULEUR[TEXTE]
    )
    pyxel.text(
        int(LARGEUR * 0.1), int(HAUTEUR * 0.6), "Press q to quit", COULEUR[TEXTE]
    )


def afficher_fin():
    """Affichage de fin"""
    pyxel.text(int(LARGEUR * 0.25), int(HAUTEUR * 0.5), "Fin de la vie", COULEUR[TEXTE])


# =========================================================
# == UPDATE
# =========================================================
def update():
    """mise à jour des variables (30 fois par seconde)"""
    # une génération par seconde
    if pyxel.btn(pyxel.KEY_S):
        jeu["menu"] = False
    if pyxel.btn(pyxel.KEY_Q):
        jeu["fin"] = True
    # mise à jour de la grille (3 fois par seconde)    
    if pyxel.frame_count % 10 == 0:
        # à compléter
        "ecrire votre code ici"


# =========================================================
# == DRAW
# =========================================================
def draw():
    """création des objets (30 fois par seconde)"""
    # vide la fenetre
    pyxel.cls(COULEUR[FONDS])
    if jeu["fin"]:
        afficher_fin()
    elif jeu["menu"]:
        afficher_menu()
    else:
        # dessin de la grille à compléter
        # dessinez chaque case avec pyxel.rect
        # un exemple avec 4 cases
        pyxel.rect(0, 0, 1, 1, COULEUR[MORT])
        pyxel.rect(LARGEUR // 2, HAUTEUR // 2, 1, 1, COULEUR[VIE])
        pyxel.rect(0, HAUTEUR - 1, 1, 1, COULEUR[VIE])
        pyxel.rect(LARGEUR - 1, HAUTEUR - 1, 1, 1, COULEUR[MORT])
        # à compléter pour rracer la grille complète


# =========================================================
# == DICTIONNAIRE GLOBAL DU JEU
# =========================================================
jeu = {
    "grille": [[MORT for __ in range(LARGEUR)] for _ in range(HAUTEUR)],
    "menu": True,
    "fin": False,
}
# =========================================================
# == PROGRAMME PRINCIPAL
# =========================================================
nom, motif = charger_fichier("glider.cells")
jeu["nom"] = nom
motif_dans_grille(0, 0, motif, jeu["grille"])
# lancement de l'application
pyxel.run(update, draw)
