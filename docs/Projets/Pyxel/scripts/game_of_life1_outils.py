"""
Jeu de la vie avec le module pyxel

https://github.com/kitao/pyxel/blob/main/doc/README.fr.md
"""

# =========================================================
# == CONSTANTES
# =========================================================
LARGEUR = 80
HAUTEUR = 80
VIE = 1
MORT = 0

# =========================================================
# == FONCTIONS DE GRILLE
# =========================================================
def copie_grille(grille):
    """Renvoie une copie profonde de la grille"""
    return [[grille[lig][col] for col in range(LARGEUR)] for lig in range(HAUTEUR)]
    
    
def test_copie_grille():
    import random
    t = [[random.randint(1, 100) for _ in range(LARGEUR)] for _ in range(HAUTEUR)]
    t2 = copie_grille(t)
    assert (t2 == t) and (id(t2) != id(t))
    print("Tests réussis pour copie_grille")
    
    
def initialiser_grille(grille, motif):
    """
    Affecte l'état VIE à toutes les cases de grille 
    listées dans motif comme couples (lig, col)
    """

def test_initialiser_grille():
    grille = [[MORT for _ in range(LARGEUR)] for __ in range(HAUTEUR)]
    motif = [(random.randint(0, 79), random.randint(0, 79)) for _ in range(50)]
    initialiser_grille(grille, motif)
    for lig in range(HAUTEUR):
        for col in range(LARGEUR):
            assert (((lig, col) in motif) and grille[lig][col] == VIE) or grille[lig][col] == MORT
    print("Tests réussis pour initialiser_grille")
    
def nombre_voisins_vivants(grille, lig, col):
    """Renvoie le nombre de voisins  vivants de la cellule en (lig, col)"""
    

def test_nombre_voisins_vivants():
    grille1 = [[VIE, MORT, VIE], [VIE, VIE, MORT], [MORT, MORT, VIE]]
    assert nombre_voisins_vivants(grille1, 1, 1) == 4
    grille2 = [[VIE, MORT, VIE], [VIE, MORT, MORT], [MORT, MORT, MORT]]
    assert nombre_voisins_vivants(grille2, 1, 1) == 3
    grille3 = [[MORT, MORT, MORT], [VIE, VIE, VIE], [MORT, MORT, MORT]]
    assert nombre_voisins_vivants(grille3, 1, 1) == 3
    print("Tests réussis pour nombre_voisins_vivants")

def evolution_cellule(grille, lig, col):
    """
    Renvoie le nouvel état (VIE ou MORT)de la cellule en (lig, col) 
    dans grille en fonction de son nombre de voisins vivants
    """
    
def test_evolution_cellule():
    grille1 = [[VIE, MORT, VIE], [VIE, VIE, MORT], [MORT, MORT, VIE]]
    assert nombre_voisins_vivants(grille1, 1, 1) == MORT
    grille2 = [[VIE, MORT, VIE], [VIE, MORT, MORT], [MORT, MORT, VIE]]
    assert nombre_voisins_vivants(grille2, 1, 1) == VIE
    grille3 = [[MORT, MORT, MORT], [VIE, VIE, VIE], [MORT, MORT, MORT]]
    assert nombre_voisins_vivants(grille3, 1, 1) == MORT
    grille4 = [[MORT, MORT, VIE], [VIE, VIE, VIE], [MORT, MORT, MORT]]
    assert nombre_voisins_vivants(grille3, 1, 1) == VIE
    grille5 = [[MORT, MORT, VIE], [VIE, VIE, MORT], [MORT, MORT, MORT]]
    assert nombre_voisins_vivants(grille3, 1, 1) == MORT
    print("Tests réussis pour nombre_evolution_cellule")

def evolution_grille(grille):
    """
    Crée une copie profonde de grille (génération n)
    Remplit cette copie avec l'évolution de chaque cellule de grille
    Renvoie cette nouvelle grille (génération n + 1)
    """
