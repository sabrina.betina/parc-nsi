"""
Jeu de la vie avec le module pyxel

https://github.com/kitao/pyxel/blob/main/doc/README.fr.md
"""

import pyxel

# =========================================================
# == CONSTANTES
# =========================================================
LARGEUR = 50
HAUTEUR = 50
VIE = 1
MORT = 0
TEXTE = 2
FONDS = 3
COULEUR = {VIE: 8, MORT: 0, TEXTE: 5, FONDS: 10}

# =========================================================
# == INITIALISATION FENETRE
# =========================================================
pyxel.init(LARGEUR, HAUTEUR, title="découverte de pyxel")


# =========================================================
# == CHARGEMENT DES IMAGES
# =========================================================
pyxel.load("game_of_life.pyxres")

# =========================================================
# == FONCTIONS 
# =========================================================

def initialiser_grille():
    """Initialise une grille de dimensions LARGEUR x HAUTEUR  avec :
        grille[lig][col] = MORT si lig et col de même parité
        sinon grille[lig][col] = VIE    
    """
    # à compléter
    # BEGIN CUT
    grille = [[MORT for col in range(LARGEUR)] for lig in range(HAUTEUR)]
    for lig in range(HAUTEUR):
        for col in range(LARGEUR):
            if (lig + col) % 2 == 0:
                grille[lig][col] = VIE
    return grille
    # END CUT
    
    
def inverser_etat(etat):
    """Inverse l'état d'une case dans une grille : MORT- > VIE et VIE -> MORT"""
    # à compléter
    # BEGIN CUT
    if etat == VIE:
        return MORT
    else:
        return VIE
    # END CUT
        
def evolution_grille(grille):
    """Mise à jour de la grille en inversant l'éat de chaque case"""
    # BEGIN CUT
    for lig in range(HAUTEUR):
        for col in range(LARGEUR):
            grille[lig][col] = inverser_etat(grille[lig][col])
    # END CUT
    return grille

# =========================================================
# == AFFICHAGES MENU / FIN
# =========================================================


def afficher_menu():
    """Affichage du menu d'accueil"""
    pyxel.text(
        int(LARGEUR * 0.25), int(HAUTEUR * 0.25), "Hello", COULEUR[TEXTE]
    )
    # à compléter pour afficher les 2 autres textes
    # BEGIN CUT
    pyxel.text(
        int(LARGEUR * 0.05), int(HAUTEUR * 0.5), "S -> start", COULEUR[TEXTE]
    )
    pyxel.text(
        int(LARGEUR * 0.05), int(HAUTEUR * 0.75), "Q -> quit", COULEUR[TEXTE]
    )
    # END CUT
    # positionnement d'images 3 x 3 dans les 4 coins
    pyxel.blt(0, 0, 0, 0, 0, 3, 3)
    # à compléter pour afficher les 3 autres images
    # BEGIN CUT
    pyxel.blt(HAUTEUR - 3, 0, 0, 8, 0, 3, 3)
    pyxel.blt(0, LARGEUR - 3, 0, 0, 5, 3, 3)
    pyxel.blt(LARGEUR - 3, HAUTEUR - 3, 0, 8, 5, 3, 3)
    # END CUT


def afficher_fin():
    """Affichage de fin"""
    pyxel.text(int(LARGEUR * 0.4), int(HAUTEUR * 0.5), "Fin", COULEUR[TEXTE])
    # positionnement d'images 8 x 8 dans les 4 coins
    # à compléter pour afficher les 4 images
    # BEGIN CUT
    pyxel.blt(0, 0, 0, 0, 8, 8, 8)
    pyxel.blt(HAUTEUR - 8, 0, 0, 0, 8, 8, 8)
    pyxel.blt(0, LARGEUR - 8, 0, 0, 8, 8, 8)
    pyxel.blt(LARGEUR - 8, HAUTEUR - 8, 0, 0, 8, 8, 8)
    # END CUT

# =========================================================
# == UPDATE
# =========================================================
def update():
    """mise à jour des variables (30 fois par seconde)"""
    # une génération par seconde
    if pyxel.btn(pyxel.KEY_S):
        jeu["menu"] = False
    # à compléter pour traiter l'appui sur la touche Q
    # BEGIN CUT
    if pyxel.btn(pyxel.KEY_Q):
        jeu["fin"] = True
    # END CUT
    if pyxel.frame_count % 30 == 0:
        # mise à jour  de la grille
        jeu["grille"] = evolution_grille(jeu["grille"])
     


# =========================================================
# == DRAW
# =========================================================
def draw():
    """création des objets (30 fois par seconde)"""
    # vide la fenetre
    pyxel.cls(COULEUR[FONDS])
    if jeu["fin"]:
        afficher_fin()
    elif jeu["menu"]:
        afficher_menu()
    # à compléter avec le dessin du damier
    # BEGIN CUT
    else:
        # dessin de la grille
        for lig in range(HAUTEUR):
            for col in range(LARGEUR):
                pyxel.rect(col, lig, 1, 1, COULEUR[jeu["grille"][lig][col]])
    # END CUT


# =========================================================
# == DICTIONNAIRE GLOBAL DU JEU
# =========================================================
jeu = {
    "grille": initialiser_grille(),
    "menu": True,
    "fin": False,
}
# =========================================================
# == PROGRAMME PRINCIPAL
# =========================================================
# lancement de l'application
pyxel.run(update, draw)
