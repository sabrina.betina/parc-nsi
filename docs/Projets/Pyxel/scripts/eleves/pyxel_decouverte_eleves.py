"""
Jeu de la vie avec le module pyxel

https://github.com/kitao/pyxel/blob/main/doc/README.fr.md
"""

import pyxel

# =========================================================
# == CONSTANTES
# =========================================================
LARGEUR = 50
HAUTEUR = 50
VIE = 1
MORT = 0
TEXTE = 2
FONDS = 3
COULEUR = {VIE: 8, MORT: 0, TEXTE: 5, FONDS: 10}

# =========================================================
# == INITIALISATION FENETRE
# =========================================================
pyxel.init(LARGEUR, HAUTEUR, title="découverte de pyxel")


# =========================================================
# == CHARGEMENT DES IMAGES
# =========================================================
pyxel.load("game_of_life.pyxres")

# =========================================================
# == FONCTIONS 
# =========================================================

def initialiser_grille():
    """Initialise une grille de dimensions LARGEUR x HAUTEUR  avec :
        grille[lig][col] = MORT si lig et col de même parité
        sinon grille[lig][col] = VIE    
    """
    # à compléter
    
    
def inverser_etat(etat):
    """Inverse l'état d'une case dans une grille : MORT- > VIE et VIE -> MORT"""
    # à compléter
        
def evolution_grille(grille):
    """Mise à jour de la grille en inversant l'éat de chaque case"""
    return grille

# =========================================================
# == AFFICHAGES MENU / FIN
# =========================================================


def afficher_menu():
    """Affichage du menu d'accueil"""
    pyxel.text(
        int(LARGEUR * 0.25), int(HAUTEUR * 0.25), "Hello", COULEUR[TEXTE]
    )
    # à compléter pour afficher les 2 autres textes
    # positionnement d'images 3 x 3 dans les 4 coins
    pyxel.blt(0, 0, 0, 0, 0, 3, 3)
    # à compléter pour afficher les 3 autres images


def afficher_fin():
    """Affichage de fin"""
    pyxel.text(int(LARGEUR * 0.4), int(HAUTEUR * 0.5), "Fin", COULEUR[TEXTE])
    # positionnement d'images 8 x 8 dans les 4 coins
    # à compléter pour afficher les 4 images

# =========================================================
# == UPDATE
# =========================================================
def update():
    """mise à jour des variables (30 fois par seconde)"""
    # une génération par seconde
    if pyxel.btn(pyxel.KEY_S):
        jeu["menu"] = False
    # à compléter pour traiter l'appui sur la touche Q
    if pyxel.frame_count % 30 == 0:
        # mise à jour  de la grille
        jeu["grille"] = evolution_grille(jeu["grille"])
     


# =========================================================
# == DRAW
# =========================================================
def draw():
    """création des objets (30 fois par seconde)"""
    # vide la fenetre
    pyxel.cls(COULEUR[FONDS])
    if jeu["fin"]:
        afficher_fin()
    elif jeu["menu"]:
        afficher_menu()
    # à compléter avec le dessin du damier


# =========================================================
# == DICTIONNAIRE GLOBAL DU JEU
# =========================================================
jeu = {
    "grille": initialiser_grille(),
    "menu": True,
    "fin": False,
}
# =========================================================
# == PROGRAMME PRINCIPAL
# =========================================================
# lancement de l'application
pyxel.run(update, draw)
