# Présentation


!!! quote "Citation"

    "**Pyxel** est un moteur de jeu vidéo rétro pour Python.
    seulement 16 couleurs peuvent être affichées et que seulement 4 sons peuvent être lus en même temps, vous pouvez vous sentir libre de créer des jeux vidéo dans le style pixel art.
    Les spécifications et les API de Pyxel sont inspirées de PICO-8 et TIC-80.
    Pyxel est un logiciel libre et open source." 

    Extrait de [github.com/kitao/pyxel](https://github.com/kitao/pyxel/blob/main/doc/README.fr.md)


En complément des exercices proposés dans ce tutoriel, deux références sur **Pyxel** :

* Le [dépôt Github](https://github.com/kitao/pyxel) avec l'[API](https://fr.wikipedia.org/wiki/Interface_de_programmation) et des informations sur l'installation, l'utilisation ...
* Les [tutoriels](https://nuitducode.github.io/DOCUMENTATION/PYTHON/01-presentation/) conçus par Laurent Abbal pour l'événement [Nuit du code](https://www.nuitducode.net/)

Pour utiliser **pyxel**, merci à Laurent Abbal de proposer les outils suivants  :

* installer sur une clef USB la distribution portable [Edupyter](https://www.edupyter.net/) qui contient le module **pyxel**
* tester son code en ligne sur <https://www.pyxelstudio.net/>  (expérimental)

# Premiers pas avec `pyxel`

On veut écrire un programme qui affiche dans une fenêtre de dimensions `50 x 50` avec :

* au lancement de l'application,  un menu d'accueil composé de trois textes et quatre images insérés dans les coins  ;
* si on appuie sur la touche `S` un écran apparaît avec un damier dont les cases changent de couleur chaque seconde ;
* si on appuie sur la touche `Q` un écran de fin apparaît composé de trois textes et quatre images insérés dans les coins.


![decouverte](images/decouverte_pyxel.gif){.center }

??? {{ exercice() }}


    === "Énoncé"

        Recopier le squelette de code ci-dessous dans un éditeur et le compléter pour satisfaire le cahier des charges fixé précédemment. On s'appuiera sur la [documentation de Pyxel]() et [le tutoriel de Laurent Abbal](https://nuitducode.github.io/DOCUMENTATION/PYTHON/01-presentation/), en particulier pour l'utilisation de [l'éditeur d'images](https://github.com/kitao/pyxel#how-to-create-resource). Les images sont dans le fichier [scripts/game_of_life.pyxres](scripts/game_of_life.pyxres) qui doit être placé dans le même répertoire que le script et qui peut être édité depuis une ligne de commandes avec `pyxel edit game_of_life.pyxres`.


        ~~~python
        """
        Découverte du module pyxel

        https://github.com/kitao/pyxel/blob/main/doc/README.fr.md
        """

        import pyxel

        # =========================================================
        # == CONSTANTES
        # =========================================================
        LARGEUR = 50
        HAUTEUR = 50
        VIE = 1
        MORT = 0
        TEXTE = 2
        FONDS = 3
        COULEUR = {VIE: 8, MORT: 0, TEXTE: 5, FONDS: 10}

        # =========================================================
        # == INITIALISATION FENETRE
        # =========================================================
        pyxel.init(LARGEUR, HAUTEUR, title="découverte de pyxel")


        # =========================================================
        # == CHARGEMENT DES IMAGES
        # =========================================================
        pyxel.load("game_of_life.pyxres")

        # =========================================================
        # == FONCTIONS 
        # =========================================================

        def initialiser_grille():
            """Initialise une grille de dimensions LARGEUR x HAUTEUR  avec :
                grille[lig][col] = MORT si lig et col de même parité
                sinon grille[lig][col] = VIE    
            """
            # à compléter
            
            
        def inverser_etat(etat):
            """Inverse l'état d'une case dans une grille : MORT- > VIE et VIE -> MORT"""
            # à compléter
                
        def evolution_grille(grille):
            """Mise à jour de la grille en inversant l'éat de chaque case"""
            return grille

        # =========================================================
        # == AFFICHAGES MENU / FIN
        # =========================================================


        def afficher_menu():
            """Affichage du menu d'accueil"""
            pyxel.text(
                int(LARGEUR * 0.25), int(HAUTEUR * 0.25), "Hello", COULEUR[TEXTE]
            )
            # à compléter pour afficher les 2 autres textes
            # positionnement d'images 3 x 3 dans les 4 coins
            pyxel.blt(0, 0, 0, 0, 0, 3, 3)
            # à compléter pour afficher les 3 autres images


        def afficher_fin():
            """Affichage de fin"""
            pyxel.text(int(LARGEUR * 0.4), int(HAUTEUR * 0.5), "Fin", COULEUR[TEXTE])
            # positionnement d'images 8 x 8 dans les 4 coins
            # à compléter pour afficher les 4 images

        # =========================================================
        # == UPDATE
        # =========================================================
        def update():
            """mise à jour des variables (30 fois par seconde)"""
            # une génération par seconde
            if pyxel.btn(pyxel.KEY_S):
                jeu["menu"] = False
            # à compléter pour traiter l'appui sur la touche Q
            if pyxel.frame_count % 30 == 0:
                # mise à jour  de la grille
                jeu["grille"] = evolution_grille(jeu["grille"])
            


        # =========================================================
        # == DRAW
        # =========================================================
        def draw():
            """création des objets (30 fois par seconde)"""
            # vide la fenetre
            pyxel.cls(COULEUR[FONDS])
            if jeu["fin"]:
                afficher_fin()
            elif jeu["menu"]:
                afficher_menu()
            # à compléter avec le dessin du damier


        # =========================================================
        # == DICTIONNAIRE GLOBAL DU JEU
        # =========================================================
        jeu = {
            "grille": initialiser_grille(),
            "menu": True,
            "fin": False,
        }
        # =========================================================
        # == PROGRAMME PRINCIPAL
        # =========================================================
        # lancement de l'application
        pyxel.run(update, draw)
        ~~~


    === "Solution"        

        ~~~python
        """
        Découverte du module pyxel

        https://github.com/kitao/pyxel/blob/main/doc/README.fr.md
        """

        import pyxel

        # =========================================================
        # == CONSTANTES
        # =========================================================
        LARGEUR = 50
        HAUTEUR = 50
        VIE = 1
        MORT = 0
        TEXTE = 2
        FONDS = 3
        COULEUR = {VIE: 8, MORT: 0, TEXTE: 5, FONDS: 10}

        # =========================================================
        # == INITIALISATION FENETRE
        # =========================================================
        pyxel.init(LARGEUR, HAUTEUR, title="découverte de pyxel")


        # =========================================================
        # == CHARGEMENT DES IMAGES
        # =========================================================
        pyxel.load("game_of_life.pyxres")

        # =========================================================
        # == FONCTIONS 
        # =========================================================

        def initialiser_grille():
            """Initialise une grille de dimensions LARGEUR x HAUTEUR  avec :
                grille[lig][col] = MORT si lig et col de même parité
                sinon grille[lig][col] = VIE    
            """
            # à compléter
            # BEGIN CUT
            grille = [[MORT for col in range(LARGEUR)] for lig in range(HAUTEUR)]
            for lig in range(HAUTEUR):
                for col in range(LARGEUR):
                    if (lig + col) % 2 == 0:
                        grille[lig][col] = VIE
            return grille
            # END CUT
            
            
        def inverser_etat(etat):
            """Inverse l'état d'une case dans une grille : MORT- > VIE et VIE -> MORT"""
            # à compléter
            # BEGIN CUT
            if etat == VIE:
                return MORT
            else:
                return VIE
            # END CUT
                
        def evolution_grille(grille):
            """Mise à jour de la grille en inversant l'éat de chaque case"""
            # BEGIN CUT
            for lig in range(HAUTEUR):
                for col in range(LARGEUR):
                    grille[lig][col] = inverser_etat(grille[lig][col])
            # END CUT
            return grille

        # =========================================================
        # == AFFICHAGES MENU / FIN
        # =========================================================


        def afficher_menu():
            """Affichage du menu d'accueil"""
            pyxel.text(
                int(LARGEUR * 0.25), int(HAUTEUR * 0.25), "Hello", COULEUR[TEXTE]
            )
            # à compléter pour afficher les 2 autres textes
            # BEGIN CUT
            pyxel.text(
                int(LARGEUR * 0.05), int(HAUTEUR * 0.5), "S -> start", COULEUR[TEXTE]
            )
            pyxel.text(
                int(LARGEUR * 0.05), int(HAUTEUR * 0.75), "Q -> quit", COULEUR[TEXTE]
            )
            # END CUT
            # positionnement d'images 3 x 3 dans les 4 coins
            pyxel.blt(0, 0, 0, 0, 0, 3, 3)
            # à compléter pour afficher les 3 autres images
            # BEGIN CUT
            pyxel.blt(HAUTEUR - 3, 0, 0, 8, 0, 3, 3)
            pyxel.blt(0, LARGEUR - 3, 0, 0, 5, 3, 3)
            pyxel.blt(LARGEUR - 3, HAUTEUR - 3, 0, 8, 5, 3, 3)
            # END CUT


        def afficher_fin():
            """Affichage de fin"""
            pyxel.text(int(LARGEUR * 0.4), int(HAUTEUR * 0.5), "Fin", COULEUR[TEXTE])
            # positionnement d'images 8 x 8 dans les 4 coins
            # à compléter pour afficher les 4 images
            # BEGIN CUT
            pyxel.blt(0, 0, 0, 0, 8, 8, 8)
            pyxel.blt(HAUTEUR - 8, 0, 0, 0, 8, 8, 8)
            pyxel.blt(0, LARGEUR - 8, 0, 0, 8, 8, 8)
            pyxel.blt(LARGEUR - 8, HAUTEUR - 8, 0, 0, 8, 8, 8)
            # END CUT

        # =========================================================
        # == UPDATE
        # =========================================================
        def update():
            """mise à jour des variables (30 fois par seconde)"""
            # une génération par seconde
            if pyxel.btn(pyxel.KEY_S):
                jeu["menu"] = False
            # à compléter pour traiter l'appui sur la touche Q
            # BEGIN CUT
            if pyxel.btn(pyxel.KEY_Q):
                jeu["fin"] = True
            # END CUT
            if pyxel.frame_count % 30 == 0:
                # mise à jour  de la grille
                jeu["grille"] = evolution_grille(jeu["grille"])
            


        # =========================================================
        # == DRAW
        # =========================================================
        def draw():
            """création des objets (30 fois par seconde)"""
            # vide la fenetre
            pyxel.cls(COULEUR[FONDS])
            if jeu["fin"]:
                afficher_fin()
            elif jeu["menu"]:
                afficher_menu()
            # à compléter avec le dessin du damier
            # BEGIN CUT
            else:
                # dessin de la grille
                for lig in range(HAUTEUR):
                    for col in range(LARGEUR):
                        pyxel.rect(col, lig, 1, 1, COULEUR[jeu["grille"][lig][col]])
            # END CUT


        # =========================================================
        # == DICTIONNAIRE GLOBAL DU JEU
        # =========================================================
        jeu = {
            "grille": initialiser_grille(),
            "menu": True,
            "fin": False,
        }
        # =========================================================
        # == PROGRAMME PRINCIPAL
        # =========================================================
        # lancement de l'application
        pyxel.run(update, draw)
        ~~~


??? video

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="pyxel-tuto" src="https://tube.ac-lyon.fr/videos/embed/2b58a2f5-c8c8-4991-91ff-f69f2c4d83a9" frameborder="0" allowfullscreen></iframe>

# Jeu de la vie

## Présentation 

__Sources :__

* Un site de Sébastien Hoareau maître de conférence en informatique à l'université de la Réunion : <https://sebhoa.gitlab.io/lupy/03_autre_puzzle/JeuDeLaVie/game_life/>
* L'article Wikipedia : <https://fr.wikipedia.org/wiki/Jeu_de_la_vie>


Le _jeu de la vie_ inventé par le mathématicien anglais J. H. Conway en 1970, est un __automate cellulaire__ en deux dimensions.

Les automates cellulaires et le jeu de la vie en particulier sont des systèmes complexes étudiés en mathématiques et en informatique théorique. Ainsi, l'automate de Conway a été prouvé _turing-complet_ c'est-à-dire qu'il peut exécuter les mêmes algorithmes qu'un ordinateur.

L'univers du jeu est une grille infinie de cases appelées cellules.

Au départ un nombre fini de cellules sont vivantes et toutes les autres sont mortes.

Chaque cellule possède un voisinage de 8 cellules voisines (sauf les cellules sur les bords dans notre simulation).

![voisinage](images/cell_et_voisines.png){.center}

!!! info

    A chaque génération, l'univers évolue selon trois règles simples de changement d'état pour chaque cellule :

    1. __Règle 1 :__  Une cellule vivante qui n'a pas au moins 2 voisines vivantes meurt par isolement.
    2. __Règle 2 :__  Une cellule vivante qui  possède 4 voisines vivantes ou plus meurt par étouffement.
    2. __Règle 3 :__  Une cellule morte qui  possède exactement trois cellules vivantes, devient vivante, sinon elle reste morte.  


## Programmation avec `pyxel` étape 1


Vous allez programmer _le jeu de la vie_ en quelques  étapes.

!!! {{ exercice() }}

    1. Pour commencer, téléchargez l'archive [materiel.zip](scripts/materiel.zip).    
    2. Dans le même répertoire, lancez un environnement de programmation  Python et créez deux scripts `game_of_life_outils.py` et `game_of_life.py`.
    



Les cellules en nombre fini seront enregistrées dans une grille de dimensions fixées par les variables globales `LARGEUR` et `HAUTEUR`.


Dans  l'exercice suivant, vous allez écrire des fonctions outils de manipulation de la grille. Copiez puis complétez le squelette fourni  dans `game_of_life_outils.py` ouvert avec votre IDE Python.

??? {{ exercice() }}

    {{ IDE("game_of_life1_outils") }}


## Programmation avec `pyxel` étape 2

Intégrez les fonctions déjà codées au squelette de code fourni dans l'exercice suivant, puis complétez les fonctions d'affichage avec le module `pyxel` pour obtenir la simulation de l'automate nommé __planeur__ (_glider_ en anglais). 
Copiez et testez le code dans `game_of_life.py` ouvert avec votre IDE Python.

!!! info

    La configuration initiale de l'automate est enregistrée dans un fichier 
    texte au format _Plain_ décrit dans <https://conwaylife.com/wiki/Plaintext>.
    Elle est chargée par le programme dans un tableau de tableaux à l'aide de la fonction `charger_fichier`.

    Deux automates sont livrés dans [materiel.zip](scripts/materiel.zip) :

    * `glider.cells`
    * `period60glidergun.cells`

Testez le code dans un environnement de programmation Python où le module [pyxel]https://github.com/kitao/pyxel) est installé.

On donne ci-dessous l'affichage _avant_ et  _après_ que le tout le code manquant soit complété.


!!! example "Avant / Après"

    === "Avant"

        ![decouverte](images/decouverte_pyxel2.gif){.center}

    === "Après"

        ![decouverte](images/decouverte_pyxel3.gif){.center}



??? {{ exercice() }}


    === "Squelette"

        ~~~python
        """
        Jeu de la vie avec le module pyxel

        https://github.com/kitao/pyxel/blob/main/doc/README.fr.md
        """

        import pyxel

        # =========================================================
        # == CONSTANTES
        # =========================================================
        LARGEUR = 80
        HAUTEUR = 80
        VIE = 1
        MORT = 0
        TEXTE = 2
        FONDS = 3
        COULEUR = {VIE: 8, MORT: 0, TEXTE: 5, FONDS: 10}

        # =========================================================
        # == INITIALISATION FENETRE
        # =========================================================
        pyxel.init(LARGEUR, HAUTEUR, title="jeu de la vie")



        # =========================================================
        # == FONCTIONS DE GRILLE
        # =========================================================
        def copie_grille(grille):
            """Renvoie une copie profonde de la grille"""
            # à compléter
            

        def initialiser_grille(grille, motif):
            """
            Affecte l'état VIE à toutes les cases de grille 
            listées dans motif comme couples (lig, col)
            """
            # à compléter


        def nombre_voisins_vivants(gille, lig, col):
            """Renvoie le nombre de voisins  vivants de la cellule en (lig, col)"""
            # à compléter


        def evolution_cellule(grille, lig, col):
            """
            Renvoie le nouvel état (VIE ou MORT)de la cellule en (lig, col) 
            dans grille en fonction de son nombre de voisins vivants
            """
            # à compléter


        def evolution_grille(grille):
            """
            Crée une copie profonde de grille (génération n)
            Remplit cette copie avec l'évolution de chaque cellule de grille
            Renvoie cette nouvelle grille (génération n + 1)
            """
            # à compléter

        # =========================================================
        # == INITIALISER LE MOTIF
        # =========================================================
        def charger_fichier(chemin):
            """
            Charger un motif stocké dans un fichier au format Plain
            https://conwaylife.com/wiki/Plaintext
            """
            f = open(chemin)
            ligne1 = f.readline().rstrip()
            titre = " ".join(ligne1.split()[1:])
            motif = []
            for ligne in f:
                if ligne[0] != "!":
                    motif.append([c for c in ligne.rstrip()])
            return titre, motif


        def motif_dans_grille(lig0, col0, motif, grille):
            """
            Recopie un motif (sous-grille) dans grille à partir de 
            la position (lig0, col0) en coin supérieur gauche
            """
            decodage = {".": MORT, "O": VIE}
            largeur_motif = len(motif[0])
            hauteur_motif = len(motif)
            assert lig0 + hauteur_motif <= len(grille) and col0 + largeur_motif <= len(
                grille[0]
            ), "Motif trop grand"
            for lig in range(lig0, lig0 + hauteur_motif):
                for col in range(col0, col0 + largeur_motif):
                    grille[lig][col] = decodage[motif[lig - lig0][col - col0]]


        # =========================================================
        # == AFFICHAGES MENU / FIN
        # =========================================================


        def afficher_menu():
            """Affichage du menu d'accueil"""
            pyxel.text(
                int(LARGEUR * 0.25), int(HAUTEUR * 0.25), f"{jeu['nom']}", COULEUR[TEXTE]
            )
            pyxel.text(
                int(LARGEUR * 0.1), int(HAUTEUR * 0.5), "Press s to start", COULEUR[TEXTE]
            )
            pyxel.text(
                int(LARGEUR * 0.1), int(HAUTEUR * 0.6), "Press q to quit", COULEUR[TEXTE]
            )
        #     # BEGIN CUT
        #     # Positionnement d'une image dans chaque coin
        #     # à compléter
        #     pyxel.blt(0, 0, 0, 0, 0, 3, 3)
        #     pyxel.blt(HAUTEUR - 3, 0, 0, 8, 0, 3, 3)
        #     pyxel.blt(0, LARGEUR - 3, 0, 0, 5, 3, 3)
        #     pyxel.blt(LARGEUR - 3, HAUTEUR - 3, 0, 8, 5, 3, 3)
        #     # END CUT


        def afficher_fin():
            """Affichage de fin"""
            pyxel.text(int(LARGEUR * 0.25), int(HAUTEUR * 0.5), "Fin de la vie", COULEUR[TEXTE])
        #     # BEGIN CUT
        #     # Positionnement d'une image dans chaque coin
        #     # à compléter
        #     pyxel.blt(0, 0, 0, 0, 8, 8, 8)
        #     pyxel.blt(HAUTEUR - 8, 0, 0, 0, 8, 8, 8)
        #     pyxel.blt(0, LARGEUR - 8, 0, 0, 8, 8, 8)
        #     pyxel.blt(LARGEUR - 8, HAUTEUR - 8, 0, 0, 8, 8, 8)
        #     # END CUT


        # =========================================================
        # == UPDATE
        # =========================================================
        def update():
            """mise à jour des variables (30 fois par seconde)"""
            # une génération par seconde
            if pyxel.btn(pyxel.KEY_S):
                jeu["menu"] = False
            if pyxel.btn(pyxel.KEY_Q):
                jeu["fin"] = True
            # mise à jour de la grille (3 fois par seconde)    
            if pyxel.frame_count % 10 == 0:
                # à compléter
                "ecrire votre code ici"


        # =========================================================
        # == DRAW
        # =========================================================
        def draw():
            """création des objets (30 fois par seconde)"""
            # vide la fenetre
            pyxel.cls(COULEUR[FONDS])
            if jeu["fin"]:
                afficher_fin()
            elif jeu["menu"]:
                afficher_menu()
            else:
                # dessin de la grille à compléter
                # dessinez chaque case avec pyxel.rect
                # un exemple avec 4 cases
                pyxel.rect(0, 0, 1, 1, COULEUR[MORT])
                pyxel.rect(LARGEUR // 2, HAUTEUR // 2, 1, 1, COULEUR[VIE])
                pyxel.rect(0, HAUTEUR - 1, 1, 1, COULEUR[VIE])
                pyxel.rect(LARGEUR - 1, HAUTEUR - 1, 1, 1, COULEUR[MORT])
                # à compléter pour tracer la grille complète


        # =========================================================
        # == DICTIONNAIRE GLOBAL DU JEU
        # =========================================================
        jeu = {
            "grille": [[MORT for __ in range(LARGEUR)] for _ in range(HAUTEUR)],
            "menu": True,
            "fin": False,
        }
        # =========================================================
        # == PROGRAMME PRINCIPAL
        # =========================================================
        # on charge l'automate enregistré dans le fichier "glider.cells" au format Plain
        # voir https://conwaylife.com/wiki/Plaintext
        nom, motif = charger_fichier("glider.cells")
        jeu["nom"] = nom
        motif_dans_grille(0, 0, motif, jeu["grille"])
        # lancement de l'application
        pyxel.run(update, draw)
        ~~~


    === "Solution"        

        ~~~python
        """
        Jeu de la vie avec le module pyxel

        https://github.com/kitao/pyxel/blob/main/doc/README.fr.md
        """

        import pyxel

        # =========================================================
        # == CONSTANTES
        # =========================================================
        LARGEUR = 80
        HAUTEUR = 80
        VIE = 1
        MORT = 0
        TEXTE = 2
        FONDS = 3
        COULEUR = {VIE: 8, MORT: 0, TEXTE: 5, FONDS: 10}

        # =========================================================
        # == INITIALISATION FENETRE
        # =========================================================
        pyxel.init(LARGEUR, HAUTEUR, title="jeu de la vie")

        # BEGIN CUT
        # =========================================================
        # == CHARGEMENT DES IMAGES
        # =========================================================
        pyxel.load("game_of_life.pyxres")
        # END CUT


        # =========================================================
        # == FONCTIONS DE GRILLE
        # =========================================================
        def copie_grille(grille):
            """Renvoie une copie profonde de la grille"""
            # à compléter
            # BEGIN CUT
            return [[grille[lig][col] for col in range(LARGEUR)] for lig in range(HAUTEUR)]
            # END CUT
            

        def initialiser_grille(grille, motif):
            """
            Affecte l'état VIE à toutes les cases de grille 
            listées dans motif comme couples (lig, col)
            """
            # à compléter
            # BEGIN CUT
            for (lig, col) in motif:
                grille[lig][col] = VIE
            # END CUT


        def nombre_voisins_vivants(grille, lig, col):
            """Renvoie le nombre de voisins  vivants de la cellule en (lig, col)"""
            # à compléter
            # BEGIN CUT
            v = 0
            for dl in range(-1, 2):
                for dc in range(-1, 2):
                    if (
                        (0 <= lig + dl < HAUTEUR)
                        and (0 <= col + dc < LARGEUR)
                        and (dl, dc) != (0, 0)
                        and grille[lig + dl][col + dc] == VIE
                    ):
                        v = v + 1
            return v
            # END CUT


        def evolution_cellule(grille, lig, col):
            """
            Renvoie le nouvel état (VIE ou MORT)de la cellule en (lig, col) 
            dans grille en fonction de son nombre de voisins vivants
            """
            # à compléter
            # BEGIN CUT
            etat = grille[lig][col]
            voisins_vivants = nombre_voisins_vivants(lig, col)
            if etat == MORT:
                if voisins_vivants == 3:
                    return VIE
                else:
                    return MORT
            else:
                if 2 <= voisins_vivants <= 3:
                    return VIE
                else:
                    return MORT
            # END CUT


        def evolution_grille(grille):
            """
            Crée une copie profonde de grille (génération n)
            Remplit cette copie avec l'évolution de chaque cellule de grille
            Renvoie cette nouvelle grille (génération n + 1)
            """
            # à compléter
            # BEGIN CUT
            grille_nouvelle = copie_grille(grille)
            for lig in range(HAUTEUR):
                for col in range(LARGEUR):
                    grille_nouvelle[lig][col] = evolution_cellule(grille, lig, col)
            return grille_nouvelle
            # END CUT

        # =========================================================
        # == INITIALISER LE MOTIF
        # =========================================================
        def charger_fichier(chemin):
            """
            Charger un motif stocké dans un fichier au format Plain
            https://conwaylife.com/wiki/Plaintext
            """
            f = open(chemin)
            ligne1 = f.readline().rstrip()
            titre = " ".join(ligne1.split()[1:])
            motif = []
            for ligne in f:
                if ligne[0] != "!":
                    motif.append([c for c in ligne.rstrip()])
            return titre, motif


        def motif_dans_grille(lig0, col0, motif, grille):
            """
            Recopie un motif (sous-grille) dans grille à partir de 
            la position (lig0, col0) en coin supérieur gauche
            """
            decodage = {".": MORT, "O": VIE}
            largeur_motif = len(motif[0])
            hauteur_motif = len(motif)
            assert lig0 + hauteur_motif <= len(grille) and col0 + largeur_motif <= len(
                grille[0]
            ), "Motif trop grand"
            for lig in range(lig0, lig0 + hauteur_motif):
                for col in range(col0, col0 + largeur_motif):
                    grille[lig][col] = decodage[motif[lig - lig0][col - col0]]


        # =========================================================
        # == AFFICHAGES MENU / FIN
        # =========================================================


        def afficher_menu():
            """Affichage du menu d'accueil"""
            pyxel.text(
                int(LARGEUR * 0.25), int(HAUTEUR * 0.25), f"{jeu['nom']}", COULEUR[TEXTE]
            )
            pyxel.text(
                int(LARGEUR * 0.1), int(HAUTEUR * 0.5), "Press s to start", COULEUR[TEXTE]
            )
            pyxel.text(
                int(LARGEUR * 0.1), int(HAUTEUR * 0.6), "Press q to quit", COULEUR[TEXTE]
            )
        #     # BEGIN CUT
        #     # Positionnement d'une image dans chaque coin
        #     # à compléter
        #     pyxel.blt(0, 0, 0, 0, 0, 3, 3)
        #     pyxel.blt(HAUTEUR - 3, 0, 0, 8, 0, 3, 3)
        #     pyxel.blt(0, LARGEUR - 3, 0, 0, 5, 3, 3)
        #     pyxel.blt(LARGEUR - 3, HAUTEUR - 3, 0, 8, 5, 3, 3)
        #     # END CUT


        def afficher_fin():
            """Affichage de fin"""
            pyxel.text(int(LARGEUR * 0.25), int(HAUTEUR * 0.5), "Fin de la vie", COULEUR[TEXTE])
        #     # BEGIN CUT
        #     # Positionnement d'une image dans chaque coin
        #     # à compléter
        #     pyxel.blt(0, 0, 0, 0, 8, 8, 8)
        #     pyxel.blt(HAUTEUR - 8, 0, 0, 0, 8, 8, 8)
        #     pyxel.blt(0, LARGEUR - 8, 0, 0, 8, 8, 8)
        #     pyxel.blt(LARGEUR - 8, HAUTEUR - 8, 0, 0, 8, 8, 8)
        #     # END CUT


        # =========================================================
        # == UPDATE
        # =========================================================
        def update():
            """mise à jour des variables (30 fois par seconde)"""
            # une génération par seconde
            if pyxel.btn(pyxel.KEY_S):
                jeu["menu"] = False
            if pyxel.btn(pyxel.KEY_Q):
                jeu["fin"] = True
            # mise à jour de la grille (3 fois par seconde)    
            if pyxel.frame_count % 10 == 0:
                # à compléter
                "ecrire votre code ici"
                # BEGIN CUT
                jeu["grille"] = evolution_grille(jeu["grille"])
                # END CUT


        # =========================================================
        # == DRAW
        # =========================================================
        def draw():
            """création des objets (30 fois par seconde)"""
            # vide la fenetre
            pyxel.cls(COULEUR[FONDS])
            if jeu["fin"]:
                afficher_fin()
            elif jeu["menu"]:
                afficher_menu()
            else:
                # dessin de la grille à compléter
                # dessinez chaque case avec pyxel.rect
                # un exemple avec 4 cases
                pyxel.rect(0, 0, 1, 1, COULEUR[MORT])
                pyxel.rect(LARGEUR // 2, HAUTEUR // 2, 1, 1, COULEUR[VIE])
                pyxel.rect(0, HAUTEUR - 1, 1, 1, COULEUR[VIE])
                pyxel.rect(LARGEUR - 1, HAUTEUR - 1, 1, 1, COULEUR[MORT])
                # à compléter pour tracer la grille complète
                # BEGIN CUT
                for lig in range(HAUTEUR):
                    for col in range(LARGEUR):
                        pyxel.rect(col, lig, 1, 1, COULEUR[jeu["grille"][lig][col]])
                # END CUT


        # =========================================================
        # == DICTIONNAIRE GLOBAL DU JEU
        # =========================================================
        jeu = {
            "grille": [[MORT for __ in range(LARGEUR)] for _ in range(HAUTEUR)],
            "menu": True,
            "fin": False,
        }
        # =========================================================
        # == PROGRAMME PRINCIPAL
        # =========================================================
        # on charge l'automate enregistré dans le fichier "glider.cells" au format Plain
        # voir https://conwaylife.com/wiki/Plaintext
        nom, motif = charger_fichier("glider.cells")
        jeu["nom"] = nom
        motif_dans_grille(0, 0, motif, jeu["grille"])
        # lancement de l'application
        pyxel.run(update, draw)
        ~~~


## Programmation avec `pyxel` étape 3

Dans l'archive [materiel.zip](scripts/materiel.zip) se trouve un fichier `game_of_life.pyxres`. Éditez ce fichier   depuis une console   avec l'éditeur d'image de [pyxel](https://github.com/kitao/pyxel/blob/main/doc/README.fr.md#comment-cr%C3%A9er-une-ressource) et la commande `pyxel edit game_of_life.pyxres`.


![editeur](images/pyxel_editor.png){.center}


!!! info

    Voir <https://nuitducode.github.io/DOCUMENTATION/PYTHON/08-tutoriel-06/> ou la [documentation](https://github.com/kitao/pyxel/blob/main/doc/README.fr.md#comment-cr%C3%A9er-une-ressource) pour l'utilisation de l'éditeur assez intuitive.

    On charge les images depuis le fichier `game_of_life.pyxres` avec `pyxel.load("game_of_life.pyxres")`.

    `pyxel.blt(0, 0, 0, 0, 0, 3, 3)` permet de placer en `(0,0)` la partie de l'image `0` contenue dans `game_of_life.pyxres`  qui se trouve en `(0,0)` et de dimensions `(3,3)`.


On veut insérer des images dans les quatre coins de la page de menu d'accueil et de fin. 




On donne ci-dessous l'affichage _avant_ et  _après_ que le tout le code manquant soit complété.

!!! example "Avant / Après"

    === "Avant"

        ![decouverte](images/decouverte_pyxel5.gif){.center}

    === "Après"

        ![decouverte](images/decouverte_pyxel4.gif){.center}


Recopiez et complétez  le squelette de code dans le fichier `game_of_life.py` ouvert avec votre IDE Python.  

??? {{ exercice() }}


    === "Squelette"

        ~~~python
        """
        Jeu de la vie avec le module pyxel

        https://github.com/kitao/pyxel/blob/main/doc/README.fr.md
        """

        import pyxel

        # =========================================================
        # == CONSTANTES
        # =========================================================
        LARGEUR = 80
        HAUTEUR = 80
        VIE = 1
        MORT = 0
        TEXTE = 2
        FONDS = 3
        COULEUR = {VIE: 8, MORT: 0, TEXTE: 5, FONDS: 10}

        # =========================================================
        # == INITIALISATION FENETRE
        # =========================================================
        pyxel.init(LARGEUR, HAUTEUR, title="jeu de la vie")


        # =========================================================
        # == CHARGEMENT DES IMAGES
        # =========================================================
        pyxel.load("game_of_life.pyxres")


        # =========================================================
        # == FONCTIONS DE GRILLE
        # =========================================================
        def copie_grille(grille):
            """Renvoie une copie profonde de la grille"""
            return [[grille[lig][col] for col in range(LARGEUR)] for lig in range(HAUTEUR)]
            

        def initialiser_grille(grille, motif):
            """
            Affecte l'état VIE à toutes les cases de grille 
            listées dans motif comme couples (lig, col)
            """
            for (lig, col) in motif:
                grille[lig][col] = VIE


        def nombre_voisins_vivants(grille, lig, col):
            """Renvoie le nombre de voisins  vivants de la cellule en (lig, col)"""
            v = 0
            for dl in range(-1, 2):
                for dc in range(-1, 2):
                    if (
                        (0 <= lig + dl < HAUTEUR)
                        and (0 <= col + dc < LARGEUR)
                        and (dl, dc) != (0, 0)
                        and grille[lig + dl][col + dc] == VIE
                    ):
                        v = v + 1
            return v


        def evolution_cellule(grille, lig, col):
            """
            Renvoie le nouvel état (VIE ou MORT)de la cellule en (lig, col) 
            dans grille en fonction de son nombre de voisins vivants
            """
            etat = grille[lig][col]
            voisins_vivants = nombre_voisins_vivants(lig, col)
            if etat == MORT:
                if voisins_vivants == 3:
                    return VIE
                else:
                    return MORT
            else:
                if 2 <= voisins_vivants <= 3:
                    return VIE
                else:
                    return MORT


        def evolution_grille(grille):
            """
            Crée une copie profonde de grille (génération n)
            Remplit cette copie avec l'évolution de chaque cellule de grille
            Renvoie cette nouvelle grille (génération n + 1)
            """
            grille_nouvelle = copie_grille(grille)
            for lig in range(HAUTEUR):
                for col in range(LARGEUR):
                    grille_nouvelle[lig][col] = evolution_cellule(grille, lig, col)
            return grille_nouvelle

        # =========================================================
        # == INITIALISER LE MOTIF
        # =========================================================
        def charger_fichier(chemin):
            """
            Charger un motif stocké dans un fichier au format Plain
            https://conwaylife.com/wiki/Plaintext
            """
            f = open(chemin)
            ligne1 = f.readline().rstrip()
            titre = " ".join(ligne1.split()[1:])
            motif = []
            for ligne in f:
                if ligne[0] != "!":
                    motif.append([c for c in ligne.rstrip()])
            return titre, motif


        def motif_dans_grille(lig0, col0, motif, grille):
            """
            Recopie un motif (sous-grille) dans grille à partir de 
            la position (lig0, col0) en coin supérieur gauche
            """
            decodage = {".": MORT, "O": VIE}
            largeur_motif = len(motif[0])
            hauteur_motif = len(motif)
            assert lig0 + hauteur_motif <= len(grille) and col0 + largeur_motif <= len(
                grille[0]
            ), "Motif trop grand"
            for lig in range(lig0, lig0 + hauteur_motif):
                for col in range(col0, col0 + largeur_motif):
                    grille[lig][col] = decodage[motif[lig - lig0][col - col0]]


        # =========================================================
        # == AFFICHAGES MENU / FIN
        # =========================================================


        def afficher_menu():
            """Affichage du menu d'accueil"""
            pyxel.text(
                int(LARGEUR * 0.25), int(HAUTEUR * 0.25), f"{jeu['nom']}", COULEUR[TEXTE]
            )
            pyxel.text(
                int(LARGEUR * 0.1), int(HAUTEUR * 0.5), "Press s to start", COULEUR[TEXTE]
            )
            pyxel.text(
                int(LARGEUR * 0.1), int(HAUTEUR * 0.6), "Press q to quit", COULEUR[TEXTE]
            )
            # Positionnement d'une image dans chaque coin
            # image dans le coin supérieur gauche
            pyxel.blt(0, 0, 0, 0, 0, 3, 3)
            # à compléter


        def afficher_fin():
            """Affichage de fin"""
            pyxel.text(int(LARGEUR * 0.25), int(HAUTEUR * 0.5), "Fin de la vie", COULEUR[TEXTE])
            # Positionnement d'une image dans chaque coin
            # à compléter


        # =========================================================
        # == UPDATE
        # =========================================================
        def update():
            """mise à jour des variables (30 fois par seconde)"""
            # une génération par seconde
            if pyxel.btn(pyxel.KEY_S):
                jeu["menu"] = False
            if pyxel.btn(pyxel.KEY_Q):
                jeu["fin"] = True
            # mise à jour de la grille (3 fois par seconde)    
            if pyxel.frame_count % 10 == 0:
                jeu["grille"] = evolution_grille(jeu["grille"])


        # =========================================================
        # == DRAW
        # =========================================================
        def draw():
            """création des objets (30 fois par seconde)"""
            # vide la fenetre
            pyxel.cls(COULEUR[FONDS])
            if jeu["fin"]:
                afficher_fin()
            elif jeu["menu"]:
                afficher_menu()
            else:
                # dessin de la grille à compléter
                # dessinez chaque case avec pyxel.rect
                # un exemple avec 4 cases
                pyxel.rect(0, 0, 1, 1, COULEUR[MORT])
                pyxel.rect(LARGEUR // 2, HAUTEUR // 2, 1, 1, COULEUR[VIE])
                pyxel.rect(0, HAUTEUR - 1, 1, 1, COULEUR[VIE])
                pyxel.rect(LARGEUR - 1, HAUTEUR - 1, 1, 1, COULEUR[MORT])
                for lig in range(HAUTEUR):
                    for col in range(LARGEUR):
                        pyxel.rect(col, lig, 1, 1, COULEUR[jeu["grille"][lig][col]])



        # =========================================================
        # == DICTIONNAIRE GLOBAL DU JEU
        # =========================================================
        jeu = {
            "grille": [[MORT for __ in range(LARGEUR)] for _ in range(HAUTEUR)],
            "menu": True,
            "fin": False,
        }
        # =========================================================
        # == PROGRAMME PRINCIPAL
        # =========================================================
        # on charge l'automate enregistré dans le fichier "glider.cells" au format Plain
        # voir https://conwaylife.com/wiki/Plaintext
        nom, motif = charger_fichier("period60glidergun.cells")
        jeu["nom"] = nom
        motif_dans_grille(0, 0, motif, jeu["grille"])
        # lancement de l'application
        pyxel.run(update, draw)
        ~~~

    === "Solution"

        ~~~python
        """
        Jeu de la vie avec le module pyxel

        https://github.com/kitao/pyxel/blob/main/doc/README.fr.md
        """

        import pyxel

        # =========================================================
        # == CONSTANTES
        # =========================================================
        LARGEUR = 80
        HAUTEUR = 80
        VIE = 1
        MORT = 0
        TEXTE = 2
        FONDS = 3
        COULEUR = {VIE: 8, MORT: 0, TEXTE: 5, FONDS: 10}

        # =========================================================
        # == INITIALISATION FENETRE
        # =========================================================
        pyxel.init(LARGEUR, HAUTEUR, title="jeu de la vie")


        # =========================================================
        # == CHARGEMENT DES IMAGES
        # =========================================================
        pyxel.load("game_of_life.pyxres")


        # =========================================================
        # == FONCTIONS DE GRILLE
        # =========================================================
        def copie_grille(grille):
            """Renvoie une copie profonde de la grille"""
            return [[grille[lig][col] for col in range(LARGEUR)] for lig in range(HAUTEUR)]
            

        def initialiser_grille(grille, motif):
            """
            Affecte l'état VIE à toutes les cases de grille 
            listées dans motif comme couples (lig, col)
            """
            for (lig, col) in motif:
                grille[lig][col] = VIE


        def nombre_voisins_vivants(grille, lig, col):
            """Renvoie le nombre de voisins  vivants de la cellule en (lig, col)"""
            v = 0
            for dl in range(-1, 2):
                for dc in range(-1, 2):
                    if (
                        (0 <= lig + dl < HAUTEUR)
                        and (0 <= col + dc < LARGEUR)
                        and (dl, dc) != (0, 0)
                        and grille[lig + dl][col + dc] == VIE
                    ):
                        v = v + 1
            return v


        def evolution_cellule(grille, lig, col):
            """
            Renvoie le nouvel état (VIE ou MORT)de la cellule en (lig, col) 
            dans grille en fonction de son nombre de voisins vivants
            """
            etat = grille[lig][col]
            voisins_vivants = nombre_voisins_vivants(lig, col)
            if etat == MORT:
                if voisins_vivants == 3:
                    return VIE
                else:
                    return MORT
            else:
                if 2 <= voisins_vivants <= 3:
                    return VIE
                else:
                    return MORT


        def evolution_grille(grille):
            """
            Crée une copie profonde de grille (génération n)
            Remplit cette copie avec l'évolution de chaque cellule de grille
            Renvoie cette nouvelle grille (génération n + 1)
            """
            grille_nouvelle = copie_grille(grille)
            for lig in range(HAUTEUR):
                for col in range(LARGEUR):
                    grille_nouvelle[lig][col] = evolution_cellule(grille, lig, col)
            return grille_nouvelle

        # =========================================================
        # == INITIALISER LE MOTIF
        # =========================================================
        def charger_fichier(chemin):
            """
            Charger un motif stocké dans un fichier au format Plain
            https://conwaylife.com/wiki/Plaintext
            """
            f = open(chemin)
            ligne1 = f.readline().rstrip()
            titre = " ".join(ligne1.split()[1:])
            motif = []
            for ligne in f:
                if ligne[0] != "!":
                    motif.append([c for c in ligne.rstrip()])
            return titre, motif


        def motif_dans_grille(lig0, col0, motif, grille):
            """
            Recopie un motif (sous-grille) dans grille à partir de 
            la position (lig0, col0) en coin supérieur gauche
            """
            decodage = {".": MORT, "O": VIE}
            largeur_motif = len(motif[0])
            hauteur_motif = len(motif)
            assert lig0 + hauteur_motif <= len(grille) and col0 + largeur_motif <= len(
                grille[0]
            ), "Motif trop grand"
            for lig in range(lig0, lig0 + hauteur_motif):
                for col in range(col0, col0 + largeur_motif):
                    grille[lig][col] = decodage[motif[lig - lig0][col - col0]]


        # =========================================================
        # == AFFICHAGES MENU / FIN
        # =========================================================


        def afficher_menu():
            """Affichage du menu d'accueil"""
            pyxel.text(
                int(LARGEUR * 0.25), int(HAUTEUR * 0.25), f"{jeu['nom']}", COULEUR[TEXTE]
            )
            pyxel.text(
                int(LARGEUR * 0.1), int(HAUTEUR * 0.5), "Press s to start", COULEUR[TEXTE]
            )
            pyxel.text(
                int(LARGEUR * 0.1), int(HAUTEUR * 0.6), "Press q to quit", COULEUR[TEXTE]
            )
            # Positionnement d'une image dans chaque coin
            # à compléter
            # BEGIN CUT    
            pyxel.blt(0, 0, 0, 0, 0, 3, 3)
            pyxel.blt(HAUTEUR - 3, 0, 0, 8, 0, 3, 3)
            pyxel.blt(0, LARGEUR - 3, 0, 0, 5, 3, 3)
            pyxel.blt(LARGEUR - 3, HAUTEUR - 3, 0, 8, 5, 3, 3)
            # END CUT


        def afficher_fin():
            """Affichage de fin"""
            pyxel.text(int(LARGEUR * 0.25), int(HAUTEUR * 0.5), "Fin de la vie", COULEUR[TEXTE])
            # Positionnement d'une image dans chaque coin
            # à compléter
            # BEGIN CUT    
            pyxel.blt(0, 0, 0, 0, 8, 8, 8)
            pyxel.blt(HAUTEUR - 8, 0, 0, 0, 8, 8, 8)
            pyxel.blt(0, LARGEUR - 8, 0, 0, 8, 8, 8)
            pyxel.blt(LARGEUR - 8, HAUTEUR - 8, 0, 0, 8, 8, 8)
            # END CUT


        # =========================================================
        # == UPDATE
        # =========================================================
        def update():
            """mise à jour des variables (30 fois par seconde)"""
            # une génération par seconde
            if pyxel.btn(pyxel.KEY_S):
                jeu["menu"] = False
            if pyxel.btn(pyxel.KEY_Q):
                jeu["fin"] = True
            # mise à jour de la grille (3 fois par seconde)    
            if pyxel.frame_count % 10 == 0:
                jeu["grille"] = evolution_grille(jeu["grille"])


        # =========================================================
        # == DRAW
        # =========================================================
        def draw():
            """création des objets (30 fois par seconde)"""
            # vide la fenetre
            pyxel.cls(COULEUR[FONDS])
            if jeu["fin"]:
                afficher_fin()
            elif jeu["menu"]:
                afficher_menu()
            else:
                # dessin de la grille à compléter
                # dessinez chaque case avec pyxel.rect
                # un exemple avec 4 cases
                pyxel.rect(0, 0, 1, 1, COULEUR[MORT])
                pyxel.rect(LARGEUR // 2, HAUTEUR // 2, 1, 1, COULEUR[VIE])
                pyxel.rect(0, HAUTEUR - 1, 1, 1, COULEUR[VIE])
                pyxel.rect(LARGEUR - 1, HAUTEUR - 1, 1, 1, COULEUR[MORT])
                for lig in range(HAUTEUR):
                    for col in range(LARGEUR):
                        pyxel.rect(col, lig, 1, 1, COULEUR[jeu["grille"][lig][col]])



        # =========================================================
        # == DICTIONNAIRE GLOBAL DU JEU
        # =========================================================
        jeu = {
            "grille": [[MORT for __ in range(LARGEUR)] for _ in range(HAUTEUR)],
            "menu": True,
            "fin": False,
        }
        # =========================================================
        # == PROGRAMME PRINCIPAL
        # =========================================================
        # on charge l'automate enregistré dans le fichier "glider.cells" au format Plain
        # voir https://conwaylife.com/wiki/Plaintext
        nom, motif = charger_fichier("period60glidergun.cells")
        jeu["nom"] = nom
        motif_dans_grille(0, 0, motif, jeu["grille"])
        # lancement de l'application
        pyxel.run(update, draw)
        ~~~