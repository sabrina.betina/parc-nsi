---
title:  Chapitre 19,  traitement de données en table
layout: parc
---



Cours de Frédéric Junier.

## Cours 

* [Cours version pdf](chapitre19/Cours/cours-tables-indexation-.pdf)
* [Cours version markdown](chapitre19/Cours/cours-tables-indexation-git.md)
* [Archive avec les exemples du cours](chapitre19/Cours/exemples_cours_tables.zip)
* [Corrigé sur Capytale](https://capytale2.ac-paris.fr/web/c/0bb5-484953)


??? video "Correction de l'exercice 2"

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="tables-donnees-cours" src="https://tube.ac-lyon.fr/videos/embed/181df983-1010-4442-8a72-a3a4cd14c6c6" frameborder="0" allowfullscreen></iframe>



## TP Recherche et tri 

* [TP version pdf](chapitre19/TP-Recherche-Tri/tp-recherche-tri-.pdf)
* [TP version markdown](chapitre19/TP-Recherche-Tri/tp-recherche-tri-git.md)
* [Ressources pour le TP](chapitre19/TP-Recherche-Tri/Ressources.zip)
* [TP avec correction sur Capytale](https://capytale2.ac-paris.fr/web/c/0bb5-484953)
* [Correction du TP version pdf](chapitre19/TP-Recherche-Tri/Correction/TP_Recherche_Tris_Correction.pdf)
* [Correction du TP version python](chapitre19/TP-Recherche-Tri/Correction/TP_Recherche_Tris_Correction.py)


## TP Fusion de tables 

* [TP version pdf](chapitre19/TP-Fusion/tp-fusion-.pdf)
* [TP version markdown](chapitre19/TP-Fusion/tp-fusion-git.md)
* [Ressources pour le TP](chapitre19/TP-Fusion/Ressources/materiel_tp_fusion.zip)
* [TP sur Capytale](https://capytale2.ac-paris.fr/web/c/50a5-11200)
* [Correction du TP version python](chapitre19/TP-Fusion/Ressources/TP_Fusion_Corrigé.py)
* [Correction du TP version notebook](https://mybinder.org/v2/gh/parc-nsi/premiere-nsi/master?filepath=chapitre19/TP-Fusion/Ressources/TP_Fusion_Corrigé.ipynb)
