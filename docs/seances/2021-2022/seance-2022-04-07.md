---
title: Séance du 07/04/2022
---

# Séance du 07/04/2022



## Chapitre traitement de données en tables



###  TP  : Fusion de tables

* [TP version pdf](../chapitre19/TP-Fusion/tp-fusion-.pdf)
* [TP version markdown](../chapitre19/TP-Fusion/tp-fusion-git.md)
* [TP sur Capytale](https://capytale2.ac-paris.fr/web/c/50a5-11200)
* [Ressources pour le TP](../chapitre19/TP-Fusion/Ressources/materiel_tp_fusion.zip)


## Présentation du projet de fin d'année, découverte du module pyxel


* [Tutoriel sur le module pyxel](../Projets/Pyxel/decouverte_pyxel.md)
* [Document de cadrage du projet final](../Projets/2021-2022/ProjetFinal/Cadrage/NSI_Presentation_Projet2022.pdf)
* [Inscription avec le mot de passe donné par l'enseignant](https://nuage-lyon.beta.education.fr/s/Fyf7EQgrpHSfCFP)



## Devoir commun

* [Entraînement sur les exercices avec juge en ligne sur Moodle](https://0690026d.moodle.ent.auvergnerhonealpes.fr/course/view.php?id=2&sectionid=3)
* Le mercredi 13 avril 2022.
* DS et corrigés donnés cette année : <https://nuage-lyon.beta.education.fr/s/sGHxPqMizZj9Txd>
