---
title: Séance du 05/05/2022
---

# Séance du 05/05/2022



# IHM sur le Web, protocole HTTP et formulaires (60 minutes)

Travail en autonomie, les élèves s'approprient tout seul le cours à partir des différents exemples et exercices.  Il vérifient leurs réponses à l'aide de la correction. Travail dans le navigateur Firefox avec les outils de développement.
Des points étapes réguliers sont prévus pour une restitution collective.

* [Lien vers le chapitre 21 avec le cours](../chapitre21.md)
*  **Premier point étape (10 minutes de travail individuel)** après l'exercice 3
*  **Second point étape (15 minutes de travail individuel)** après l'exemple 1 => **Point de cours 2** sur les _Méthodes de passage des paramètres : GET ou POST_ puis **Exercice 4** en collectif.
*  **Second point étape (30 minutes de travail individuel)** après le **Point de cours 3** sur les _éléments de formulaire HTML_ et l'exercice 5 => Synthèse en commun.

# Travail sur le projet de fin d'année


* [Tutoriel sur le module pyxel](../Projets/Pyxel/decouverte_pyxel.md)
* [Document de cadrage du projet final](../Projets/2021-2022/ProjetFinal/Cadrage/NSI_Presentation_Projet2022.pdf)
* [Inscription avec le mot de passe donné par l'enseignant](https://nuage-lyon.beta.education.fr/s/Fyf7EQgrpHSfCFP)

