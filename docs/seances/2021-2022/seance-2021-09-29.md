---
title: Séance du 29/09/2021
---

# Séance du 29/09/2021

## Évaluation orale du mini-projet

Consigne : 3 minutes de présentation du projet par élève.

Noté sur deux points (sur un total de 10) : 1 pour la forme (expression, posture, structure du discours) et l'autre pour le fonds.



## Découverte de la plateforme Upylab 

* Accès : [https://upylab2.ulb.ac.be](https://upylab2.ulb.ac.be)  :   quatre exercices du parcours :
    * minimum de 2 valeurs
    * teste égalité parmi 3 nombres
    * 4 parcours avec range
    * seuil

## Chapitre 2 : révisions HTML/CSS

!!! info "Le point sur les langages HTML et CSS"

    * [Synthèse de cours](../chapitre2/memo/MemoHTML-CSS-2020.pdf)  ➡️ pages 1 à 3
    * Activités proposées dans [le mini-site portable](https://filesender.renater.fr/?s=download&token=27f5ebec-3d2f-4649-82f1-5a1080b71d9f)  :
        * Dans `site/premiere_page_html/html_balises.html` faire les exercices 2, 3, 4 et 5 : lire les consignes sur le mini-site et traiter les exercices dans Capytale :
            * [exercice 3](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/0f0c-61116)
       




## A faire pour la semaine prochaine :

1. Sur [France ioi](http://www.france-ioi.org/) finir le chapitre 4.
2. S'inscrire sur la plateforme [Upylab](https://upylab2.ulb.ac.be) avec le lien fourni et si besoin s'entraîner sur les deux premiers exercices des chapitres 1 (Variables), 2 (Conditionnelle), 3 (boucle for), 4 (boucle while) et 5 (fonctions)


## A faire pour le 21/10/2021 :

* Projet HTML/CSS de réalisation d'un mini-site sur un thème en relation avec l'histoire de l'informatique. Chosir un sujet par binôme.
* Le cahier des charges, les [sujets](../sujets-html-css.md) et des exemples de ressources ont été proposés aux élèves.
* [Modèle de site fourni](../rojets/Projets2020/HTML-CSS-Histoire/modele.zip)
