---
title: Séance du 01/12/2021
---

# Séance du 01/12/2021

## Automatismes :

* [Automatismes 2 à 7](../automatismes/automatismes-2021-2022.md)



## Découverte de la ligne de commandes

* [TP Terminus (version originale de Charles Poulmaire)](../chapitre9/terminus/terminus.md)
* Memento shell
    * [Memento shell version pdf](../chapitre9/memento-shell/memento-shell-.pdf)
    * [Memento shell version markdown](../chapitre9/memento-shell/memento-shell-git.md)
    * [Matériel  pour les exemples du memento](../chapitre9/memento-shell/sandbox.zip)


## Révisions pour jeudi 02/12

* Interrogation écrite (30 minutes) sur les tableaux à 1 ou 2 dimensions et la représentations des entiers (jusqu'à ce qui a été vu mercredi)

