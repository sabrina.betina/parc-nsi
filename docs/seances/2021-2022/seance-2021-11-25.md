---
title: Séance du 25/11/2021
---

# Séance du 25/11/2021

## Automatismes :

* [Automatisme 8 (correction) et automatisme 5](../automatismes/automatismes-2021-2022.md)



##  Chapitre 6 : représentation des entiers

* [Cours / TP en version pdf](../chapitre8/Chapitre6-ReprésentationEntiers-2021V1.pdf)
* [Cours / TP en notebook Python sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/2076-172112)
* [Matériel en archive zip (fichiers Python, Notebook)](../chapitre8/materiel.zip)
* [Corrigé du cours / TP en version pdf](../chapitre8/Cours_Representation_Entiers_Correction.pdf)
* [Corrigé des exercices de calcul sans python](../chapitre8/Chapitre6-ReprésentationEntiers-2021V1-corrige.pdf)
* [Corrigé du cours en notebook Python sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/a963-171873)

## Faire le point

* QCM sur la représentation des entiers non signés:
    * [Énoncé](https://genumsi.inria.fr/qcm.php?h=9ecc083484829c2ee620207e0d28e5d8)
    * [Corrigé](https://genumsi.inria.fr/qcm-corrige.php?cle=MTM7NzA7OTQ7MTg1OzE5Mjs0NjA7MTM1NDsxNDE0OzE2MzA7MTY1OQ==)

* QCM sur la représentation des entiers signés :
    * [Énoncé](https://genumsi.inria.fr/qcm.php?h=fd8be6ad3707a92245056f06086e6767)
    * [Corrigé](https://genumsi.inria.fr/qcm-corrige.php?cle=MTU3OzE1ODsxNjk7NDA4OzQ2Mjs0NjM7MTM1MzsxNDkzOzE0OTU7MTYzNg==)

## Révisions pour mercredi 01/12

* Interrogation écrite (30 minutes) sur les tableaux à 1 ou 2 dimensions et la représentations des entiers (jusqu'à ce qui a été vu mercredi)

