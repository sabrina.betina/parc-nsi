---
title: Séance du 06/09/2022
---

# Séance du 06/09/2022

## Consignes et méthode de travail :

* Matériel :
    * Un classeur avec des feuilles pour écrire et des pochettes transparentes pour ranger les cours
    * Mettre le site <https://parc-nsi.github.io/premiere/> dans les favoris de son navigateur, navigation responsive adaptée aux smartphone : toutes les ressources (cours, corrigés) sont publiés et disponibles sur ce site. Trois rubriques à retenir :
        * La [progression](https://parc-nsi.github.io/premiere/) avec les liens vers les chapitres
        * La liste des [séances](https://parc-nsi.github.io/premiere/seances/) détaillées qui sont ensuite copiées dans Pronote.
        * La rubrique [Automatismes](https://parc-nsi.github.io/premiere/automatismes/) avec des liens vers des QCM externes et des exercices pour travailler les automatismes.
    * Une clef USB de 8 Go minimum, cet [article](https://www.boulanger.com/ref/872118) n'est pas cher.
    * Le manuel Hachette NSI version papier fourni par la région de référence `978-2-01-786630-5`, accessible en ligne sur <https://mesmanuels.fr/acces-libre/3813624>

* Méthode de travail :
    * D'une séance à l'autre : relire le cours, faire les exercices
    * Pendant la séance : alternance de temps d'activités et de synthèse, travail sur des projets    à rendre
    * Évaluations :
        * Rendu de mini-projet ou de projet plus conséquent (pendant les vacances) : travail en classe et à la maison en binôme, évaluations écrites ou orales
        * Formatives sous forme d'interrogations courtes (format QCM) ou d'exposés oral (histoire de l'informatique, synthèse de cours)
        * Sommatives sous forme de devoir d'une heure ou de TP noté
        * Autres (exposés, création d'un tuto video)


## Chapitre 1: constructions de bases en langage Python


1. Rejoindre le groupe __ParcPremiereNSI__ sur <http://www.france-ioi.org>  (se créer un compte d'abord) et commencer le chapitre 1 (affichage) du niveau 1.
2. --Activité d'introduction pages 28 et 29 du manuel NSI Hachette. [Correction](https://gitlab.com/frederic-junier/parc-nsi/-/raw/master/docs/chapitre1/exos/corrections/activitep28.py) dans l'environnement [Idle](https://www.python.org/downloads/) ou [Spyder (dans Anaconda)](https://www.anaconda.com/products/individual)--
3. [TP 1](../chapitre1/TP1/1NSI-Chap1-Variables-TP1-.pdf) : instructions; expressions, variables, erreurs. Sur Capytale < <https://capytale2.ac-paris.fr/web/c/a5e6-644695/mcer>



## A faire pour jeudi 08/09 :

1. Rejoindre le groupe __ParcPremiereNSI__ sur <http://www.france-ioi.org>  (se créer un compte d'abord) et finir le chapitre 1 (affichage) du niveau 1.
2. Faire le __niveau 1  chapitre 1(affichage)__ du parcour général de <http://www.france-ioi.org>
3. Faire les exos 5 et 6 du TP1 sur Capytale <https://capytale2.ac-paris.fr/web/c/a5e6-644695/mcer>
4. Réfléchir à  un sujet de mini-projet sur [MiniProjet1](../Projets/MiniProjets2022-2023/NSI-MiniProjets1-2022-2023-Sujets.pdf), s'inscrire par binôme sur <https://nuage03.apps.education.fr/index.php/s/WYT8ED4GL8NDAZ3>. A rendre pour le vendredi 23/09 dans la zone de dépôt sur Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=964>




![map](ressources/map.jpg){:align="center"}