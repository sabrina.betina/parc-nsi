---
title: Séance du 22/09/2022
---

# Séance du 22/09/2022


## Chapitre 1: constructions de bases en langage Python


    
!!! example "Répétitions conditionnées"


    Exercices du chapitre _Répétitions conditionnées_ <http://www.france-ioi.org/algo/chapter.php?idChapter=649>


!!! example "Fonctions"


    * Exercice [Meilleur tarif](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=1153) dans la rubrique _Juge en ligne_ du cours moodle
    * Exercice [Super Avare](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=1154) dans la rubrique _Juge en ligne_ du cours moodle
    * Exercice [Nature d'un triangle](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=1156) dans la rubrique _Juge en ligne_ du cours moodle



!!! info Mini-Projet

   Travail sur le [MiniProjet1](../Projets/MiniProjets2022-2023/NSI-MiniProjets1-2022-2023-Sujets.pdf), inscription par binôme sur <https://nuage03.apps.education.fr/index.php/s/WYT8ED4GL8NDAZ3>. A rendre pour le vendredi 23/09 dans la zone de dépôt sur Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=964>





# DS n°1 (45 minutes) sur les constructions de base en Python


## A faire pour mardi 27/09 :


!!! abstract "Mini-Projet"
    
     Travailler sur son  mini-projet : énoncé sur [MiniProjet1](../Projets/MiniProjets2022-2023/NSI-MiniProjets1-2022-2023-Sujets.pdf), inscription par binôme sur <https://nuage03.apps.education.fr/index.php/s/WYT8ED4GL8NDAZ3>. A rendre pour le mardi 27/09 dans la zone de dépôt sur Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=964> au plus tard  le mardi 27/09 à 23h59.

     Présentation orale du projet (entre 120 et 180 secondes maximum par personne) le mardi 4/10.





