---
title: Thème Boucle bornée (for)
---

{% include 'abbreviations.md' %}


!!! tip "Exercice"

    Écrire un programme Python qui affiche "`au moins deux égales`" si au moins deux valeurs des variables `a`, `b` et `c` sont  égales et  "`au moins deux différentes`" sinon.
    


{{IDE("exo4_tests")}} 

[Correction](corr_exo4_tests.py)
