---
title: Thème Boucle non bornée (while)
---

{% include 'abbreviations.md' %}


!!! tip "Exercice"

    Écrire un programme Python de deux lignes de code au plus, qui affiche tous les entiers pairs entre 0 et 20 inclus dans l'ordre croissant (un par ligne).
    


{{IDE("exo4_range")}} 

[Correction](exo4_range_corr.py)
